# React Yield/From

[![Coverage Status](https://coveralls.io/repos/gitlab/Deathrage/react-statements/badge.svg?branch=master)](https://coveralls.io/gitlab/Deathrage/react-statements?branch=master)
[![Gitlab pipeline status (branch)](https://img.shields.io/gitlab/pipeline/Deathrage/react-statements/master.svg)](https://gitlab.com/Deathrage/react-statements)

Proper rendering tool to handle arrays, array-like objects, iterators, iterables, generators and more!

Includes TypeScript definitions

## Quick Start

`npm install --save react-yield-from`

Simple pattern:

```
import Yield from "react-yield-from";

<Yield from={/* source */}>{/* Handler yielded result */}</Yield>
```

## Props and types

### Yield

| Name     | Type                                                                                                                | Required | Description                                                                |
| -------- | ------------------------------------------------------------------------------------------------------------------- | -------- | -------------------------------------------------------------------------- |
| from     | `Array|ArrayLike|Iterable|Iterator|Generator|GeneratorFunction|(() => Array|ArrayLike|Iterable|Iterator|Generator)` | `true`   | Source that will be iterated through by the Yield component                |
| children | `(result => ReactNode) | ReactNode`                                                                                 | `true`   | Transformer function or component that will be rendered for each iteration |

<br/>

### Yielded Component

When React component is used instead of function as a child, yielded value will be propagated to it. Interface `YieldedComponentProps` might be used in TypeScript. When using any non-`object` child (not being `ReactElement`) no value gets propagated, ie. children like `string`, `Array` or `null`;

| Name    | Type  | Required | Description                             |
| ------- | ----- | -------- | --------------------------------------- |
| yielded | `any` | `false`  | Propagated value of the iteration cycle |

<br/>

## Handling and transforming

Results recovered from the source will be propagated to the children. Propagated value can be either handled by `function` or by React component.

```
<Yield from={["Joe", "Billy"]}>
	{name => <span>{name}</span>}
</Yield>
```

When using React component, the value will be injected to the props as `yielded` prop.

```
const MyComponent = ({ yielded }) => <span>{yielded}</span>;

<Yield from={["Joe", "Billy"]}>
	<MyComponent />
</Yield>
```

Component can even ignore propagated value. This might be handy when using Yield/From as a `while` loop.

## Sources

Yield/From combines power of `for ... of` and `Array.from`. Thus Yield/From is capable of processing almost anything in some way.

### Arrays

```
<div>
	<h2>Current score is:</h2>
	<Yield from={[{name: "Bob", score: 50}, {name: "John", score: 25]}>
		{({name, score}) => <span>{name}:{score}</span>}
	</Yield>
</div>
```

### Array Likes

Array like object are those with `length` property and indexed keys. Native `arguments` and `NodeList` are one of the most common array like objects.

```
const myArrayLike = {
	0: "Joe",
	1: "Hans",
	length: 2
};

<Yield from={myArrayLike}>
	{name => <span>{name}</span>}
</Yield>
```

### Iterables

Iterables are objects that implement iterable protocol by having `[Symbol.iterator]` method. Native example might me `array` or `string`.

### Iterators

Iterators are objects implementing iterator protocol. Method `[Symbol.iterator]` returns iterator object. Iterator objects are those who implement `next` method.

```
const myIterator = {
	next: () => ({
		value: 'foo',
		done: false
	})
}

<Yield from={myIterator}>
	{name => <span>{name}</span>}
</Yield>
```

### Generators

Generators are objects that are created by invoking `function*` (function declared with asterisk symbol). Generators implement iterator and iterable protocol.

```
function* MyGenerator() {
	yield 10;
	yield 50;
}

const myGenerator = MyGenerator();

<Yield from={myGenerator}>
	{name => <span>{name}</span>}
</Yield>
```

### Factories and Generator functions

Any of the previous types can be also used as source when wrapped in function. If function is passed in `from` props, it will get invoked without any arguments.

```
function Factory() {
	return [5, 10];
}

<Yield from={Factory}>
	{name => <span>{name}</span>}
</Yield>
```

Generator function can be used in this fashion as well.

```
function* MyGenerator() {
	yield 10;
	yield 50;
}

<Yield from={MyGenerator}>
	{name => <span>{name}</span>}
</Yield>
```

## Built in loops

Yield/From comes with few prebuilt iterators in order to simulate behaviour of loops such as `for`, `while` or `do ... while` as well as for creating value ranges. They are available through exported object `Iterators`;

### Range

Creates range between two values including `min` and excluding `max`.

```
import Yield, { Iterators } from "react-yield-from";

<Yield from={Iterators.range(0,5,1)}>
	{name => <span>{name}</span>}
</Yield>
```

Arguments as follows: `min`, `max` and `step`.

### For Loop

Simulates behaviour of `for(let i = 0; i<5; i++)`

```
import Yield, { Iterators } from "react-yield-from";

<Yield from={Iterators.forLoop(0, i => i < 5 , i => i + 1)}>
	{name => <span>{name}</span>}
</Yield>
```

Arguments as follows: `start`, `condition` and `raise`.

### While Loop

Simulates behaviour of `while(condition)`

```
import Yield, { Iterators } from "react-yield-from";

<Yield from={Iterators.whileLoop(() => shouldContinue())}>
	{name => <span>{name}</span>}
</Yield>
```

Arguments as follows: `condition`.

### Do While Loop

Simulates behaviour of `do ... while(condition)`

```
import Yield, { Iterators } from "react-yield-from";

<Yield from={Iterators.doWhileLoop(() => shouldContinue())}>
	{name => <span>{name}</span>}
</Yield>
```

Arguments as follows: `condition`.
