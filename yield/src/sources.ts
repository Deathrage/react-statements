/**
 * @description Generates range iterable, min is included, max is excluded
 * @param  {number} min
 * @param  {number} max
 * @param  {number} step
 */
export function range(
  min: number,
  max: number,
  step: number
): Iterable<number> {
  const result: number[] = [];
  for (let i = min; i < max; i += step) {
    result.push(i);
  }
  return result;
}
/**
 * @description Generates iterator that simulates while
 * @param  {()=>boolean} condition
 */
export function whileLoop(condition: () => boolean): Iterator<undefined> {
  return {
    next: function () {
      return { value: undefined, done: !condition() };
    },
  };
}
/**
 * @description Generates iterator that simulates do while
 * @param  {()=>boolean} condition
 */
export function doWhileLoop(condition: () => boolean): Iterator<undefined> {
  // As at least one run of do while has to happen, it starts as falsy
  // Signals that the next iteration will break
  let breakNext: boolean = false;
  return {
    next: function () {
      // No value is yield; when breakNext is signaled stops iterator by signalling donees
      const currentResult = { value: undefined, done: breakNext };
      // once breakNext becomes truthy do not ever again check condition
      breakNext = breakNext || !condition();
      // As at least one iteration has to go, it's important to store value before changing the flag
      return currentResult;
    },
  };
}
/**
 * @description Generates iterator that simulates for
 * @param  {TType} from
 * @param  {(current:TType)=>boolean} condition
 * @param  {(current:TType)=>TType} step
 */

export function forLoop<TType>(
  from: TType,
  condition: (current: TType) => boolean,
  step: (current: TType) => TType
): Iterator<TType> {
  let current = from;
  let broken = false;

  const innerIterator = whileLoop(function whileCondition() {
    // Checks whether current iteration should carry on
    // as soon as the loop has been broken, never again check the condition
    const conditionResult = broken || condition(current);
    // Raises by step
    current = step(current);
    // It's important to check condition before raising current value
    // as for cycle works like while when not satisfying the condition means breaking
    return conditionResult;
  });

  return {
    next: function () {
      // Store current before it gets raised by next
      const currentCache = current;
      // Get next iteration of while loop
      const innerResult = innerIterator.next();
      return {
        ...innerResult,
        // If the while is done, return undefined, otherwise return stored current value (before raise)
        value: innerResult.done
          ? ((undefined as unknown) as TType)
          : currentCache,
      };
    },
  };
}
