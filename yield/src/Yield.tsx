import React, { ReactNode, Fragment } from "react";
import { YieldProps, YieldChild } from "./types";
import { getChildFactory, defactorize, toArray } from "./helpers";

interface YieldPropsWithChildren<TType> extends YieldProps<TType> {
  children: YieldChild<TType>;
}

export function Yield<TType = any>({
  from,
  children,
}: YieldPropsWithChildren<TType>) {
  // Determines strategy to propagate the result
  const childFactory = getChildFactory(children);

  // If passed as generator function or other factory, unwrap it
  const iterable = defactorize(from);

  // Will be transpiler by TSC to ES5 friendly code
  const results: ReactNode[] = toArray(iterable).map(childFactory);

  // Empty fragment in order to skip that array/key inconvenience
  return results.length ? React.createElement(Fragment, {}, ...results) : null;
}
