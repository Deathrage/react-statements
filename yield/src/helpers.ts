import {
  YieldChild,
  ChildFactory,
  YieldedComponentProps,
  YieldSource,
  YieldCollectionSource,
  YieldIteratorSource,
} from "./types";
import { isValidElement, cloneElement } from "react";

/**
 * @description Determines the strategy to propagate what was yielded
 */
export function getChildFactory<TType>(
  child: YieldChild<TType>
): ChildFactory<TType> {
  if (typeof child === "function") {
    return function childFunctionFactory(result) {
      return child(result);
    };
  } else if (
    typeof child === "object" &&
    isValidElement<YieldedComponentProps<TType>>(child)
  ) {
    return function childObjectFactory(result) {
      return cloneElement(child as any, {
        yielded: result,
      });
    };
  }
  return function childPureFactory() {
    return child;
  };
}

/**
 * @description If source is factory (generator function or function) unwraps it
 */
export function defactorize<TType>(
  source: YieldSource<TType>
): YieldCollectionSource<TType> {
  return typeof source === "function"
    ? (source() as YieldCollectionSource<TType>)
    : source;
}

export function isIterator<TType>(
  source: YieldCollectionSource<TType>
): source is YieldIteratorSource<TType> {
  return (
    typeof source === "object" &&
    "next" in source &&
    typeof source.next === "function"
  );
}

export function isArrayLike<TType>(
  source: YieldCollectionSource<TType>
): source is ArrayLike<TType> {
  return typeof source === "object" && "length" in source;
}

/**
 * @description Transform any possible collection source to array
 */
export function toArray<TType>(from: YieldCollectionSource<TType>): TType[] {
  const yields: TType[] = [];
  try {
    // When implements iterator protocol (method next)
    if (isIterator(from)) {
      let current = from.next();
      while (!current.done) {
        yields.push(current.value);
        current = from.next();
      }
    }
    // If generator, array, iterable, or array like method Array.from can turn all in array
    else {
      for (const item of Array.from(from)) {
        yields.push(item);
      }
    }
  } finally {
    return yields;
  }
}
