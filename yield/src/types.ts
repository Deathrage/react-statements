import { ReactNode, ReactElement } from "react";

/**
 * @description Source that implements Iterable protocol, [Symbol.iterator]
 */
export type YieldIterableSource<TType> = Iterable<TType> | TType[];
/**
 * @description Source that is indexed object with length property
 */
export type YieldArrayLikeSource<TType> = ArrayLike<TType> | TType[];
/**
 * @description Source that implements iterator protocol (method next)
 */
export type YieldIteratorSource<TType> = Iterator<TType> | Generator<TType>;
/**
 * @description All collection sources, iterators, array and array-likes, iterables
 */
export type YieldCollectionSource<TType> =
  | YieldIterableSource<TType>
  | YieldArrayLikeSource<TType>
  | YieldIteratorSource<TType>;
/**
 * @description Factory that might be used to recover collection source, can be generator or any factory
 */
export type YieldFactorySource<TType> =
  | GeneratorFunction
  | (() => YieldCollectionSource<TType>);

export type YieldSource<TType> =
  | YieldFactorySource<TType>
  | YieldCollectionSource<TType>;

export interface YieldProps<TType> {
  from: YieldSource<TType>;
}

export interface YieldedComponentProps<TType> {
  yielded?: TType;
}

export type ChildFactory<TType> = (result: TType) => ReactNode;

export type YieldChild<TType> =
  | ((result: TType) => ReactNode)
  | ReactElement<YieldedComponentProps<TType>>
  | ReactNode;
