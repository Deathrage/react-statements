import {
  getChildFactory,
  defactorize,
  isIterator,
  toArray,
} from "../src/helpers";
import React, { ReactElement } from "react";
import { YieldedComponentProps } from "../src/types";

describe("Tests of helpers", () => {
  /**
   * Method getChildFactory
   */
  test("Get child factory as a function", () => {
    const factory = getChildFactory((inc) => "foo" + inc);
    expect(factory("bar")).toEqual("foobar");
  });
  test("Get child factory as a valid react element", () => {
    const factory = getChildFactory(<div />);
    const result = factory("bar") as ReactElement<
      YieldedComponentProps<string>
    >;
    expect(() => {
      expect(result).toEqual(<div />);
      expect(result.props.yielded).toEqual("bar");
    });
  });
  test("Get child factory as a invalid react element", () => {
    const factory = getChildFactory({ this: "is not valid" });
    expect(factory("foo")).toEqual({ this: "is not valid" });
  });
  test("Get child factory as a non object", () => {
    const factory = getChildFactory(50);
    expect(factory("foo")).toEqual(50);
  });
  test("Get child factory as a non object", () => {
    const factory = getChildFactory(50);
    expect(factory("foo")).toEqual(50);
  });
  /**
   * Method defactorize
   */
  test("Defactorize factory", () => {
    const raw = defactorize(() => [1, 2, 3]);
    expect(raw).toEqual([1, 2, 3]);
  });
  test("Defactorize non factory", () => {
    const raw = defactorize([1, 2, 3]);
    expect(raw).toEqual([1, 2, 3]);
  });
  /**
   * Method isIterator
   */
  test("Is generator an iterator", () => {
    function* generator() {
      yield 5;
    }
    expect(isIterator(generator())).toBeTruthy();
  });
  test("Is custom iterator an iterator", () => {
    expect(
      isIterator({ next: () => ({ value: 5, done: false }) })
    ).toBeTruthy();
  });
  test("Is non iterator an iterator", () => {
    expect(isIterator({ not: "an iterator" } as any)).toBeFalsy();
  });
  test("Is non iterator (with non method next) an iterator", () => {
    expect(
      isIterator({ next: "has next yet is not iterator" } as any)
    ).toBeFalsy();
  });
  /**
   * Method toArray
   */
  test("Generator to array", () => {
    function* generator() {
      yield 5;
      yield 10;
    }
    expect(toArray(generator())).toEqual([5, 10]);
  });
  test("Iterable (array) to array", () => {
    expect(toArray([5, 10])).toEqual([5, 10]);
  });
  test("Iterable (string) to array", () => {
    expect(toArray("hesoyam")).toEqual(["h", "e", "s", "o", "y", "a", "m"]);
  });
  test("Iterable (custom) to array", () => {
    const iterable = {
      [Symbol.iterator]: () => {
        let offset = 1;
        return {
          next: () => {
            return {
              value: offset++,
              done: offset > 5,
            };
          },
        };
      },
    };
    expect(toArray(iterable)).toEqual([1, 2, 3, 4]);
  });
  test("Iterator to array", () => {
    function factory() {
      let offset = 1;
      return {
        next: () => ({ value: offset++, done: offset > 5 }),
      };
    }
    expect(toArray(factory())).toEqual([1, 2, 3, 4]);
  });
  test("Array like to array", () => {
    expect(toArray({ 0: "s", 1: "b", length: 2 })).toEqual(["s", "b"]);
  });
  test("Not suitable to array", () => {
    expect(toArray({} as any)).toEqual([]);
  });
});
