import { range, whileLoop, doWhileLoop, forLoop } from "../src/sources";

describe("Test predefined sources", () => {
  /**
   * Range
   */
  test("Test range source", () => {
    const sut = range(0, 5, 1);
    expect(sut).toEqual([0, 1, 2, 3, 4]);
  });
  /**
   * While Loop
   */
  test("Test while loop source", () => {
    let pointer = 0;
    const sut = whileLoop(() => {
      pointer++;
      return pointer < 5;
    });
    const iterableSut = {
      [Symbol.iterator]: () => sut,
    };
    expect([...iterableSut]).toEqual([
      undefined,
      undefined,
      undefined,
      undefined,
    ]);
  });
  test("Test while loop source, falsy by default", () => {
    const sut = whileLoop(() => false);
    const iterableSut = {
      [Symbol.iterator]: function () {
        return sut;
      },
    };
    expect([...iterableSut]).toEqual([]);
  });
  /**
   * Do While Loop
   */
  test("Test do while loop source", () => {
    let pointer = 0;
    const sut = doWhileLoop(() => {
      pointer++;
      return pointer < 5;
    });
    const iterableSut = {
      [Symbol.iterator]: () => sut,
    };
    expect([...iterableSut]).toEqual([
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
    ]);
  });
  test("Test while loop source, falsy by default", () => {
    const sut = doWhileLoop(() => false);
    const iterableSut = {
      [Symbol.iterator]: function () {
        return sut;
      },
    };
    expect([...iterableSut]).toEqual([undefined]);
  });
  /**
   * For Loop
   */
  test("Test while loop source", () => {
    const mirror: number[] = [];
    for (let i = 0; i < 5; i++) {
      mirror.push(i);
    }
    const sut = forLoop(
      0,
      (c) => c < 5,
      (c) => c + 1
    );
    const iterableSut = {
      [Symbol.iterator]: function () {
        return sut;
      },
    };
    expect([...iterableSut]).toEqual(mirror);
  });
  test("Test while loop source, falsy by default", () => {
    const mirror: number[] = [];
    for (let i = 0; i < 0; i++) {
      mirror.push(i);
    }
    const sut = forLoop(
      0,
      (c) => c < 0,
      (c) => c + 1
    );
    const iterableSut = {
      [Symbol.iterator]: function () {
        return sut;
      },
    };
    expect([...iterableSut]).toEqual(mirror);
  });
});
