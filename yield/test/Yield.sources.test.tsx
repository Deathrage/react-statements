import React from "react";
import renderer from "react-test-renderer";

import Yield from "../src";
import { Iterators } from "../src";

describe("Yield component using sources", () => {
  test("Range source", () => {
    const sut = (
      <div>
        <Yield from={Iterators.range(0, 5, 1)}>
          {(result) => <span>{result}</span>}
        </Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  test("While loop", () => {
    let offset = 0;
    const sut = (
      <div>
        <Yield
          from={Iterators.whileLoop(() => {
            offset++;
            return offset < 5;
          })}
        >
          {(result) => <span>{result}</span>}
        </Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  test("Falsy while loop", () => {
    const sut = (
      <div>
        <Yield from={Iterators.whileLoop(() => false)}>
          {(result) => <span>{result}</span>}
        </Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  test("Do while loop", () => {
    let offset = 0;
    const sut = (
      <div>
        <Yield
          from={Iterators.doWhileLoop(() => {
            offset++;
            return offset < 5;
          })}
        >
          {() => <span>Many!</span>}
        </Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  test("Falsy do while loop", () => {
    const sut = (
      <div>
        <Yield from={Iterators.doWhileLoop(() => false)}>
          {() => <span>Only one</span>}
        </Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  test("For loop", () => {
    const sut = (
      <div>
        <Yield
          from={Iterators.forLoop(
            0,
            (i) => i < 5,
            (i) => i + 1
          )}
        >
          {() => <span>5 for loops results!</span>}
        </Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
});
