import React from "react";
import renderer from "react-test-renderer";

import Yield, { YieldedComponentProps } from "../src";

describe("Yield component", () => {
  /**
   * Function based array render
   */
  test("Render simple array with simple function", () => {
    const sut = (
      <div>
        <Yield from={[1, 2, 3]}>{(result) => result}</Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  test("Render simple empty array with simple function", () => {
    const sut = (
      <div>
        <Yield from={[]}>{(result) => result}</Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  test("Render array like with simple function", () => {
    const sut = (
      <div>
        <Yield from={{ 0: "s", 1: "b", length: 2 }}>{(result) => result}</Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  test("Render empty array like with simple function", () => {
    const sut = (
      <div>
        <Yield from={{ 0: "s", 1: "b", length: 0 }}>{(result) => result}</Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  /**
   * Component based array render
   */
  test("Render simple array with simple component", () => {
    const YieldCatcher = ({ yielded }: YieldedComponentProps<number>) => {
      return <h2>{yielded}</h2>;
    };

    const sut = (
      <div>
        <Yield from={[1, 2, 3]}>
          <YieldCatcher />
        </Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });

  test("Render empty array with simple component", () => {
    const YieldCatcher = ({ yielded }: YieldedComponentProps<number>) => {
      return <h2>{yielded}</h2>;
    };

    const sut = (
      <div>
        <Yield from={[]}>
          <YieldCatcher />
        </Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  test("Render simple array with ignorant component", () => {
    const IgnorantComponent = () => {
      return <h2>I am such an ignorant!</h2>;
    };

    const sut = (
      <div>
        <Yield from={[1, 2, 3]}>
          <IgnorantComponent />
        </Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  /**
   * Component based array like render
   */
  test("Render array like with simple component", () => {
    const YieldCatcher = ({ yielded }: YieldedComponentProps<number>) => {
      return <h2>{yielded}</h2>;
    };

    const sut = (
      <div>
        <Yield from={{ 0: "s", 1: "b", length: 2 }}>
          <YieldCatcher />
        </Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  /**
   * Function based generator render
   */
  test("Render generator with simple function", () => {
    function* generator() {
      yield "Macho";
      yield "Man";
    }

    const sut = (
      <div>
        <Yield from={generator}>{(result) => result}</Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  /**
   * Component based generator render
   */
  test("Render generator with simple function", () => {
    function* generator() {
      yield "Macho";
      yield "Man";
    }

    const YieldCatcher = ({ yielded }: YieldedComponentProps<number>) => {
      return <h2>{yielded}</h2>;
    };

    const sut = (
      <div>
        <Yield from={generator}>{<YieldCatcher />}</Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  /**
   * Function based iterable render
   */
  test("Render iterable with complex function", () => {
    const iterable = {
      [Symbol.iterator]: function () {
        let offset = "s";
        return {
          next: function () {
            offset += "a";
            return { value: offset, done: offset === "saaa" };
          },
        };
      },
    };

    const sut = (
      <div>
        <Yield from={iterable}>{(result) => <span>{result}</span>}</Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  /**
   * Component based iterator render
   */
  test("Render iterator with simple component", () => {
    let offset = "s";
    const iterator = {
      next: function () {
        offset += "a";
        return { value: offset, done: offset === "saaa" };
      },
    };

    const YieldCatcher = ({ yielded }: YieldedComponentProps<number>) => {
      return <h2>{yielded}</h2>;
    };

    const sut = (
      <div>
        <Yield from={iterator}>
          <YieldCatcher />
        </Yield>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
  /**
   * Re-rendering
   */
  test("Re-render component with different generators", () => {
    function* firstGenerator() {
      yield "first";
      yield "to";
      yield "come";
    }
    function* secondGenerator() {
      yield "last";
      yield "to";
      yield "go";
    }

    let offset = 0;
    const Sut = () => {
      offset++;
      return (
        <div>
          <Yield from={offset > 2 ? firstGenerator() : secondGenerator()}>
            {(result) => <span>{result}</span>}
          </Yield>
        </div>
      );
    };

    const tree = renderer.create(Sut());
    tree.update(Sut());

    const result = tree.toJSON();
    expect(result).toMatchSnapshot();
  });
});
