import React, { ReactNode } from "react";
import { Case } from "react-when-then";
import { FalsyProps } from "./types";

interface FalsyPropsWithChildren extends FalsyProps {
  children: ReactNode;
}

export function Falsy({ children }: FalsyPropsWithChildren) {
  return <Case when={false}>{children}</Case>;
}
