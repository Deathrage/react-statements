import React, { ReactElement, Children, SFC } from "react";
import { Switch, CaseProps, DefaultProps } from "react-when-then";
import { TernaryProps, TruthyProps, FalsyProps } from "./types";
import { isFunction, assert, unwrapElement } from "./helpers";
import { Truthy } from "./Truthy";
import { Falsy } from "./Falsy";

interface TernaryPropsWithChildren extends TernaryProps {
  children: (ReactElement<TruthyProps> | ReactElement<FalsyProps>)[];
}

export function Ternary({
  condition = false,
  children,
}: TernaryPropsWithChildren) {
  const childrenArray = Children.toArray(children) as ReactElement<
    FalsyProps | TruthyProps
  >[];

  assert(
    childrenArray.length > 0,
    `There has to be one ${nameof(Truthy)} and ${nameof(
      Falsy
    )} component withing ${nameof(Ternary)} component`
  );

  assert(
    childrenArray.every((child) => {
      if (typeof child != "object") return false;
      return (child.type as SFC) === Truthy || (child.type as SFC) === Falsy;
    }),
    `Only components of type ${nameof(Truthy)} or ${nameof(
      Falsy
    )} can be specified directly inside the switch.`
  );

  const truthies = childrenArray.filter(
    ({ type: childType }) => childType === Truthy
  );

  assert(
    truthies.length === 1,
    `Exactly one ${nameof(
      Truthy
    )} component has to be specified within ${nameof(Ternary)} component.`
  );

  const falsies = childrenArray.filter(
    ({ type: childType }) => childType === Falsy
  );

  assert(
    falsies.length === 1,
    `Exactly one ${nameof(Falsy)} component has to be specified within ${nameof(
      Ternary
    )} component.`
  );

  const builtTruthy = unwrapElement(truthies[0]);
  const builtFalsy = unwrapElement(falsies[0]);

  const result = isFunction(condition) ? condition() : condition;
  return (
    <Switch condition={!!result}>
      {builtTruthy as ReactElement<CaseProps<boolean>>}
      {builtFalsy as ReactElement<DefaultProps>}
    </Switch>
  );
}

Ternary.Truthy = Truthy;
Ternary.Falsy = Falsy;
