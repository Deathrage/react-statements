/// <reference types="ts-nameof" />
export { Ternary } from "./Ternary";
export { Truthy } from "./Truthy";
export { Falsy } from "./Falsy";
export { TruthyProps, FalsyProps, TernaryProps } from "./types";
