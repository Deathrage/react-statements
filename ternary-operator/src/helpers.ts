import { ReactElement, SFC } from "react";
import { FalsyProps, TruthyProps } from "./types";

export function isFunction<Signature = Function>(
  value: any
): value is Signature {
  return typeof value === "function";
}

export function assert(condition: boolean, message: string): void {
  if (!condition) throw new Error(message);
}

export function unwrapElement({
  key,
  type: ctor,
  props,
}: ReactElement<FalsyProps> | ReactElement<TruthyProps>) {
  assert(isFunction(ctor), `Element's constructor is not a function!`);

  // Return inner react element
  const builtTruthy = (ctor as SFC)({ ...props }) as ReactElement<any>;
  assert(!!builtTruthy, "No component was unwrapped!");

  return { ...builtTruthy, key };
}
