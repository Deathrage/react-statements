export interface TernaryProps {
  condition: boolean | (() => boolean);
}

export interface TruthyProps {}

export interface FalsyProps {}
