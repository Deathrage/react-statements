import React, { ReactNode } from "react";
import { Case } from "react-when-then";
import { TruthyProps } from "./types";

interface TruthyPropsWithChildren extends TruthyProps {
  children: ReactNode;
}

export function Truthy({ children }: TruthyPropsWithChildren) {
  return <Case when={true}>{children}</Case>;
}
