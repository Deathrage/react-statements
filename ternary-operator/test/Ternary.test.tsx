import React from "react";
import renderer from "react-test-renderer";

import { Ternary, Truthy, Falsy } from "../src";

describe("Ternary operator", () => {
  test("Proper ternary (truthy)", () => {
    const sut = (
      <div>
        <Ternary condition={true}>
          <Truthy>Truthy</Truthy>
          <Falsy>Falsy</Falsy>
        </Ternary>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });

  test("Proper ternary (falsy)", () => {
    const sut = (
      <div>
        <Ternary condition={false}>
          <Truthy>Truthy</Truthy>
          <Falsy>Falsy</Falsy>
        </Ternary>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });

  test("Proper ternary (with function)", () => {
    const sut = (
      <div>
        <Ternary condition={() => true}>
          <Truthy>Truthy</Truthy>
          <Falsy>Falsy</Falsy>
        </Ternary>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });

  test("Improper ternary (missing any child)", () => {
    const SpoofedTernary = Ternary as any;
    const sut = (
      <div>
        <SpoofedTernary condition={false} />
      </div>
    );

    expect(() => {
      renderer.create(sut);
    }).toThrow(
      `There has to be one ${nameof(Truthy)} and ${nameof(
        Falsy
      )} component withing ${nameof(Ternary)} component`
    );
  });

  test("Improper ternary (having exotic child)", () => {
    const SpoofedTernary = Ternary as any;
    const sut = (
      <div>
        <SpoofedTernary condition={false}>
          <Truthy>Foo</Truthy>
          <Falsy>Bar</Falsy>
          <div>Invader!</div>
        </SpoofedTernary>
      </div>
    );

    expect(() => {
      renderer.create(sut);
    }).toThrow(
      `Only components of type ${nameof(Truthy)} or ${nameof(
        Falsy
      )} can be specified directly inside the switch.`
    );
  });

  test("Improper ternary (multiple truthies)", () => {
    const SpoofedTernary = Ternary as any;
    const sut = (
      <div>
        <SpoofedTernary condition={false}>
          <Truthy>Foo</Truthy>
          <Truthy>Another one</Truthy>
          <Falsy>Bar</Falsy>
        </SpoofedTernary>
      </div>
    );

    expect(() => {
      renderer.create(sut);
    }).toThrow(
      `Exactly one ${nameof(
        Truthy
      )} component has to be specified within ${nameof(Ternary)} component.`
    );
  });

  test("Improper ternary (no falsy)", () => {
    const SpoofedTernary = Ternary as any;
    const sut = (
      <div>
        <SpoofedTernary condition={false}>
          <Truthy>Foo</Truthy>
        </SpoofedTernary>
      </div>
    );

    expect(() => {
      renderer.create(sut);
    }).toThrow(
      `Exactly one ${nameof(
        Falsy
      )} component has to be specified within ${nameof(Ternary)} component.`
    );
  });

  test("Truthy and Falsy are subcomponents of Ternary", () => {
    const sut = (
      <Ternary condition={true}>
        <Ternary.Truthy>truthy</Ternary.Truthy>
        <Ternary.Falsy>falsy</Ternary.Falsy>
      </Ternary>
    );
    expect(sut).toBeTruthy();
  });
});
