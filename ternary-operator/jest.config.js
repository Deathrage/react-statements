module.exports = {
  globals: {
    "ts-jest": {
      compiler: "ttypescript",
    },
  },
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
  roots: ["./test"],
  setupFilesAfterEnv: ["@testing-library/jest-dom/extend-expect"],
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
};
