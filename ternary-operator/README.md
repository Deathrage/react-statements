# React Ternary Operator

[![Coverage Status](https://coveralls.io/repos/gitlab/Deathrage/react-statements/badge.svg?branch=master)](https://coveralls.io/gitlab/Deathrage/react-statements?branch=master)
[![Gitlab pipeline status (branch)](https://img.shields.io/gitlab/pipeline/Deathrage/react-statements/master.svg)](https://gitlab.com/Deathrage/react-statements)

Ternary operator implemented in JSX.

Includes TypeScript definitions

`npm install --save react-ternary-operator`

Checkout [React When->Then](https://www.npmjs.com/package/react-when-then) to unlock full potential of JSX conditions.

```
import { Ternary, Truthy, Falsy } from 'react-ternary-operator';

export const MyComponent = (props) => (
  <Ternary condition={props.items.length === 10}>
    <Truthy>
      <h1>You've got 10 all 10 items!</h1>
    </Truthy>
    <Falsy>
      <h1>There are more items to go!</h1>
    </Falsy>
  </Ternary>
);
```

## Components

**Ternary**

Ternary component encapsulates the logic of the ternary operator.

Props
|Name|Type|Required|Description|
|---|---|---|---
|condition|`boolean` or `() => boolean`|`true`|_Either expression that resoles to boolean or `Function` that returns boolean._

<br/>

**Truthy**

Encapsulates the elements returned when condition resolves to truthy.

Defines no props.

**Falsy**

Encapsulates the elements returned when condition resolves to falsy.

Defines no props.
