# React When->Then

[![Coverage Status](https://coveralls.io/repos/gitlab/Deathrage/react-statements/badge.svg?branch=master)](https://coveralls.io/gitlab/Deathrage/react-statements?branch=master)
[![Gitlab pipeline status (branch)](https://img.shields.io/gitlab/pipeline/Deathrage/react-statements/master.svg)](https://gitlab.com/Deathrage/react-statements)

Are you tired of using inline JavaScript expression inside your JSX? Does it make the code unreadable? Have you ever though to yourself "**there's got to be a better way**"?

Includes TypeScript definitions

## Quick Start

`npm install --save react-when-then`

Simple pattern

```
<Case when={/* when */}>{/* then */}</Case>
```

Usable as standalone

```
import { Case } from 'react-when-then';

export const LoadingIndicator = (props) => (
	<Case when={props.loading}>
		<Loader/>
	</Case>
);
```

Or inside larger statements

```
import { Switch, Case, Default } from 'react-when-them';

export const UserProfile = (props) => {
	return (
		<Switch>
			<Case when={props.age < 18}>
				<p>Underaged!</p>
			</Case>
			<Case when={props.age <= 21}>
				<p>Young adult!</p>
			</Case>
			<Default>
				<p>Adult!</p>
			</Default>
		</Switch>
	);
}
```

<br/>

# Components

When->Then is made of three components: Switch, Case, Default.

<br/>

## Case

Case works as an `if` statement. Cases can be used by themselves or in combination.

```
	<Case when={/* Conition /*}>
		{/* Then */}
	</Case>
```

When used standalone they might be used as:

```
	export const MyComponent = (props) => (
		<div>
			<Case when={props.warning}>
				// show warning
			</Case>
			<div>
				// rest of the page
			</div>
		</div>
	);
```

Using multiple standalone Cases can be interpreted as **if() {} if() {} if() {}**

```
	if (first) { return /* First */ }
	<Case when={first}>
		// First
	</Case>
	if (second) { return /* Second*/ }
	<Case when={second}>
		// Second
	</Case>
```

**TypeScript generic arguments**
|Name|Required|Default|Description|
|---|---|---|---|
ConditionValue | `false`|`boolean`| _When used with Switch, the type of the when is shared with Switch's condition_

<br/>

**Props**
| Name | Type | Required | Default | Description |
|---|---|---|---|---|
|when|`ConditionValue` or `any`|`true`|N/A|_When used standalone, truthfulness is asserted by ternal `when ? then : null`. When used with Switch truthfulness is asserted by the Switch's condition._

<br/>

## Switch

Multiple Cases might be wrapped by a Switch. This construction can either be interpreted as `switch case default` statement of `if else` statement. Switch iterates through all Cases. First Case resolved as turthy is returned. When no Case is resolved as truthy the Default is returned instead (if specified).

**If Else**

```
<Switch>
	// if (first && second){ return /* First then */ }
	<Case when={first && second}>
		// First then
	</Case>
	// else if (second) { return /* Second then */ }
	<Case when={second}>
		// Second then
	</Case>
	// else { return /* Default */ }
	<Default>
		// Default
	</Default>
</Switch>
```

**Switch Case Default**

```
<Switch condition={user.role}>
	<Case when={UserRoles.Admin}>
		<Redirect to={Rotes.AdminHub}/>
	</Case>
	<Case when={UserRoles.User}>
		<Redirect to={Rotes.UserDashboard}/>
	</Case>
	<Default>
		<Redirect to={Routes.Unauthorized}/>
	</Default>
</Switch>
```

**TypeScript generic arguments**
|Name|Required|Default|Description|
|---|---|---|---|
ConditionValue | `false`|`boolean`| _Specifies tye of the condition_

<br/>

**Props**
| Name | Type | Required | Default | Description |
|---|---|---|---|---|
|condition|`ConditionValue` or `any`|`false`|`true`|_When unspecified the Cases will be tested against `true`. When specified Cases will be tested by `===`_
|loose|`boolean`|`false`|`false`|Changes assertion from `===` to `==`. Will make condition `""` hit `0` etc...
|booleanify|`boolean`|`false`|`false`|Will test condition and cases as `!!condition == !!when`. Usefull when testing for presence of a value as `!!{} == !!true` in `<Case when={data}>`

<br/>

## Default

Specifies default fallback for the Switch when no Cases are truthy. Can be specified only once per switch. Doesn't have to be specified at all.

Has neither props nor any generic arguments.

```
	<Default>
		// Default fallback inside Switch
	</Default>
```
