import React, { Children, ReactNode, Fragment } from "react";
import { DefaultProps } from "./types";
import { assert } from "./helpers";

interface DefaultPropsWithChildren extends DefaultProps {
  children: ReactNode;
}

export function Default({ children }: DefaultPropsWithChildren) {
  assert(
    Children.count(children) > 0,
    `No children specified for component ${nameof(
      Default
    )}, are you sure that ${nameof(Default)} has to be specified?`
  );

  return <Fragment>{children}</Fragment>;
}
