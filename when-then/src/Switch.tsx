import React, {
  ReactElement,
  Children,
  cloneElement,
  SFC,
  Fragment,
} from "react";
import { SwitchProps, CaseProps, DefaultProps } from "./types";
import { Case } from "./Case";
import { Default } from "./Default";
import { assert } from "./helpers";

interface SwitchWithChildrenProps<ConditionValue>
  extends SwitchProps<ConditionValue> {
  children:
    | ReactElement<CaseProps<ConditionValue>>
    | (ReactElement<CaseProps<ConditionValue>> | ReactElement<DefaultProps>)[];
}

export function Switch<ConditionValue = boolean>({
  condition,
  children,
  loose = false,
  booleanify = false,
}: SwitchWithChildrenProps<ConditionValue>) {
  // When no condition is specified fallback to boolean (true)
  const sanitizedCondition = condition ?? true;

  const childrenArray = Children.toArray(children) as ReactElement<
    CaseProps<ConditionValue> | DefaultProps
  >[];
  assert(
    childrenArray.every((child) => {
      if (typeof child != "object") return false;
      return (child.type as SFC) === Case || (child.type as SFC) === Default;
    }),
    `Only components of type ${nameof(Case)} or ${nameof(
      Default
    )} can be specified directly inside the switch.`
  );

  const cases = childrenArray.filter(
    ({ type: childType }) => (childType as SFC) === Case
  ) as ReactElement<CaseProps<ConditionValue>>[];

  assert(
    cases.length > 0,
    `No ${nameof(Case)} components specified for ${nameof(
      Switch
    )} component, are you sure that ${nameof(Switch)} has to be specified?`
  );

  const defaults = childrenArray.filter(
    ({ type: childType }) => (childType as SFC) === Default
  ) as ReactElement<DefaultProps>[];

  assert(
    defaults.length <= 1,
    `More than one (${defaults.length}) ${nameof(
      Default
    )} components specified for ${nameof(
      Switch
    )} component, are you sure that you need that many?`
  );

  const truthyCase = cases.find(({ props: { when: childWhen } }) => {
    if (booleanify) return !!childWhen === !!sanitizedCondition;
    if (loose) return childWhen == sanitizedCondition;
    return childWhen === sanitizedCondition;
  });

  return (
    <Fragment>
      {truthyCase
        ? cloneElement(
            // When controller by switch, give the truthy case forcibly truthy condition
            truthyCase as unknown as ReactElement<CaseProps<boolean>>,
            {
              when: true,
            }
          )
        : defaults[0] || null}
    </Fragment>
  );
}

Switch.Case = Case;
Switch.Default = Default;
