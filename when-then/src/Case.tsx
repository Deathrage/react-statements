import React, { Fragment, ReactNode, Children } from "react";
import { CaseProps } from "./types";
import { assert } from "./helpers";

interface CasePropsWithChildren<ConditionValue>
  extends CaseProps<ConditionValue> {
  children?: ReactNode;
}

export function Case<ConditionValue = boolean>({
  when,
  children,
}: CasePropsWithChildren<ConditionValue>) {
  assert(
    Children.count(children) > 0,
    `No children specified for component ${nameof(
      Case
    )}, are you sure that ${nameof(Case)} has to be specified?`
  );

  return <Fragment>{when ? children : null}</Fragment>;
}
