export interface CaseProps<ConditionValue> {
  when: ConditionValue;
}

export interface SwitchProps<ConditionValue> {
  condition?: ConditionValue;
  loose?: boolean;
  booleanify?: boolean;
}

export interface DefaultProps {}
