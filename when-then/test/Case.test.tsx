import React from "react";
import renderer from "react-test-renderer";

import { Case } from "../src";

describe(`Test ${nameof(Case)} component`, () => {
  test("Show when true", () => {
    const sut = (
      <div>
        <Case when={true}>
          <span>Hello world!</span>
        </Case>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });

  test("Hide when false", () => {
    const sut = (
      <div>
        <Case when={false}>
          <span>Hello world!</span>
        </Case>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });

  test("Multiple", () => {
    const sut = (
      <div>
        <Case when={true}>
          <span>First</span>
        </Case>
        <Case when={false}>
          <span>Second</span>
        </Case>
        <Case when={true}>
          <span>Third</span>
        </Case>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });

  test("Throw when no then", () => {
    const sut = (
      <div>
        <Case when={false} />
      </div>
    );

    expect(() => {
      renderer.create(sut);
    }).toThrowError(
      `No children specified for component ${nameof(
        Case
      )}, are you sure that ${nameof(Case)} has to be specified?`
    );
  });

  test("Auto booleanify", () => {
    const sut = (
      <div>
        <Case when={{ data: "something" }}>
          <span>Yes condition was met!</span>
        </Case>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
});
