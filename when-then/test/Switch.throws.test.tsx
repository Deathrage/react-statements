import React from "react";
import renderer from "react-test-renderer";

import { Case, Switch, Default } from "../src";

describe(`Test asserts of ${nameof(Switch)} component`, () => {
  test("Throw when no cases (or default)", () => {
    const SpoofedSwitch = Switch as any;
    const sut = (
      <div>
        <SpoofedSwitch />
      </div>
    );

    expect(() => {
      renderer.create(sut);
    }).toThrow(
      `No ${nameof(Case)} components specified for ${nameof(
        Switch
      )} component, are you sure that ${nameof(Switch)} has to be specified?`
    );
  });

  test("Throw when no cases (but one default)", () => {
    const sut = (
      <div>
        <Switch>
          <Default>Hello!</Default>
        </Switch>
      </div>
    );

    expect(() => {
      renderer.create(sut);
    }).toThrow(
      `No ${nameof(Case)} components specified for ${nameof(
        Switch
      )} component, are you sure that ${nameof(Switch)} has to be specified?`
    );
  });

  test("Throw when too many defaults", () => {
    const sut = (
      <div>
        <Switch>
          <Case when={true}>Hi!</Case>
          <Case when={true}>Hi!</Case>
          <Default>Hello!</Default>
          <Default>Hello!</Default>
        </Switch>
      </div>
    );

    expect(() => {
      renderer.create(sut);
    }).toThrow(
      `More than one (2) ${nameof(Default)} components specified for ${nameof(
        Switch
      )} component, are you sure that you need that many?`
    );
  });

  test("Throw when strange children specified", () => {
    const sut = (
      <div>
        <Switch>
          <Case when={true}>Hi!</Case>
          <Default>Hello!</Default>
          <div>What am I?</div>
        </Switch>
      </div>
    );

    expect(() => {
      renderer.create(sut);
    }).toThrow(
      `Only components of type ${nameof(Case)} or ${nameof(
        Default
      )} can be specified directly inside the switch.`
    );
  });
});
