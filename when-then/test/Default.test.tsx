import React from "react";
import renderer from "react-test-renderer";

import { Default } from "../src";

describe(`Test ${nameof(Default)} component`, () => {
  test("Throw when empty", () => {
    const SpoofedDefault = Default as any;
    const sut = (
      <div>
        <SpoofedDefault />
      </div>
    );

    expect(() => {
      renderer.create(sut);
    }).toThrow(
      `No children specified for component ${nameof(
        Default
      )}, are you sure that ${nameof(Default)} has to be specified?`
    );
  });

  test("Show children", () => {
    const sut = (
      <div>
        <Default>Hello;</Default>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });
});
