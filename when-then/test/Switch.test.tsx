import React from "react";
import renderer from "react-test-renderer";

import { Case, Switch, Default } from "../src";

describe(`Test ${nameof(Switch)} component`, () => {
  test("Displays no case (without default)", () => {
    const sut = (
      <div>
        <Switch>
          <Case when={false}>First</Case>
          <Case when={false}>Second</Case>
        </Switch>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });

  test("Displays only first truthy case (1st)", () => {
    const sut = (
      <div>
        <Switch>
          <Case when={true}>First</Case>
          <Case when={true}>Second</Case>
        </Switch>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });

  test("Displays only first truthy case (3rd)", () => {
    const sut = (
      <div>
        <Switch>
          <Case when={false}>First</Case>
          <Case when={false}>Second</Case>
          <Case when={true}>Third</Case>
          <Case when={true}>Fourth</Case>
        </Switch>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });

  test("Displays default", () => {
    const sut = (
      <div>
        <Switch>
          <Case when={false}>First</Case>
          <Case when={false}>Second</Case>
          <Default>Default</Default>
        </Switch>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });

  test('Asserts strongly 0 against "", false and 0 (should show Zero)', () => {
    const sut = (
      <div>
        <Switch condition={0}>
          <Case when={""}>Empty quotes</Case>
          <Case when={false}>False</Case>
          <Case when={0}>Zero</Case>
          <Default>Default</Default>
        </Switch>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });

  test('Asserts loosely ""  against 0, false and "" (should show Zero)', () => {
    const sut = (
      <div>
        <Switch condition={""} loose>
          <Case when={0}>Zero</Case>
          <Case when={false}>False</Case>
          <Case when={""}>Empty quotes</Case>
          <Default>Default</Default>
        </Switch>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });

  test("Asserts against enum", () => {
    enum TestEnum {
      FOO,
      BAR,
      BAZ,
    }

    const sut = (
      <div>
        <Switch<TestEnum> condition={TestEnum.BAR}>
          <Case<TestEnum> when={TestEnum.FOO}>FOO</Case>
          <Case<TestEnum> when={TestEnum.BAR}>BAR</Case>
          <Case<TestEnum> when={TestEnum.BAZ}>BAZ</Case>
          <Default>Default</Default>
        </Switch>
      </div>
    );

    const result = renderer.create(sut).toJSON();
    expect(result).toMatchSnapshot();
  });

  test("Asserts against realistic conditions booleanify", () => {
    let data: { user: string } | null = null;
    let loading = !data;

    const sut = () => (
      <div>
        <Switch booleanify>
          <Case when={!loading && data}>Data loaded!!!</Case>
          <Default>DataLoading</Default>
        </Switch>
      </div>
    );

    const render = renderer.create(sut());
    const firstResult = render.toJSON();

    data = { user: "Eduardo " };
    loading = !data;

    render.update(sut());
    const secondResult = render.toJSON();

    expect({
      firstResult,
      secondResult,
    }).toMatchSnapshot();
  });

  test("Asserts against realistic conditions strongly", () => {
    let data: { user: string } | null = null;
    let loading = !data;

    const sut = () => (
      <div>
        <Switch>
          <Case when={!loading && !!data}>Data loaded!!!</Case>
          <Default>DataLoading</Default>
        </Switch>
      </div>
    );

    const render = renderer.create(sut());
    const firstResult = render.toJSON();

    data = { user: "Eduardo " };
    loading = !data;

    render.update(sut());
    const secondResult = render.toJSON();

    expect({
      firstResult,
      secondResult,
    }).toMatchSnapshot();
  });

  test("Asserts that Case and Default are subcomponents of Switch", () => {
    const sut = (
      <Switch>
        <Switch.Case when={true}>foo</Switch.Case>
        <Switch.Default>bar</Switch.Default>
      </Switch>
    );
    expect(sut).toBeTruthy();
  });
});
